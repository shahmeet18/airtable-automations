# %%
import pandas as pd
import requests
import json
import time

# %%
# Define API key, base key, table name, and CSV file path
api_key = 'your_api_key'
base_key = 'your_base_key'
table_name = 'your_table_name'
csv_file_path = 'your_csv_file_path'
unique_column_name = 'your_unique_column_name'  # Update this to the unique column name


# %%
# Define headers
headers = {
    'Authorization': 'Bearer ' + api_key,
    'Content-Type': 'application/json'
}

# %%
# Define URL
url = f"https://api.airtable.com/v0/{base_key}/{table_name}"

# %%
# Get all records from Airtable
airtable_records = []
offset = None
while True:
    params = {'pageSize': 100}
    if offset:
        params['offset'] = offset
    response = requests.get(url, headers=headers, params=params)
    data = response.json()
    airtable_records.extend(data['records'])
    time.sleep(0.2)  # Respect the rate limit
    offset = data.get('offset')
    if not offset:
        break

# %%
# Convert airtable records to a set of unique IDs for easy comparison
airtable_unique_ids = set(record['fields'].get(unique_column_name) for record in airtable_records)

# %%
# Load data from CSV file
df = pd.read_csv(csv_file_path, encoding='latin1')
csv_records = df.to_dict('records')

# %%
# Check for missing records
missing_records = []
for csv_record in csv_records:
    if csv_record.get(unique_column_name) not in airtable_unique_ids:
        for key, value in csv_record.items():
            if isinstance(value, float):
                if pd.isna(value):
                    csv_record[key] = None
                elif value == float('inf'):
                    csv_record[key] = 1.79E308  # JSON's maximum finite number
                elif value == float('-inf'):
                    csv_record[key] = -1.79E308  # JSON's minimum finite number
        missing_records.append({"fields": csv_record})
        if len(missing_records) == 10:
            response = requests.post(url, headers=headers, json={"records": missing_records})
            if response.status_code != 200:
                print(f"Failed to insert records. Error: {response.text}")
            missing_records = []
            time.sleep(0.2)  # Respect the rate limit

# %%
# Append any remaining missing records
if missing_records:
    response = requests.post(url, headers=headers, json={"records": missing_records})
    if response.status_code != 200:
        print(f"Failed to insert records. Error: {response.text}")


